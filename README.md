# Finantial Transactions API

A api for finantial transactions using Cloud Functions, Firestore and Postgresql.

## Implantação

1. create system user:

   - `sudo adduser <user>`

2. create db user, with same name and pass:

   - `sudo -u postgres createuser --interactive`
   - `sudo -u postgres psql`
   - `\password <user>`
   - `\q`

3. create database:
   - `sudo -u postgres createdb <dbname>`

2) create tables:

   - `sudo -u postgres psql`
   - `\DROP TABLE IF EXISTS users`
   - `\CREATE TABLE users (id SERIAL PRIMARY KEY, uid VARCHAR(255), email VARCHAR(255), credits DOUBLE)`
   - `\DROP TABLE IF EXISTS history`
   - `\CREATE TABLE history (id SERIAL PRIMARY KEY, uid VARCHAR(255), useremail VARCHAR(255), email VARCHAR(255), type VARCHAR(10), value VARCHAR(20))`
   - `\q`

## Comandos

```
$ cd front-end && npm install
$ cd functions && npm install
$ npm install -g firebase-tools
$ firebase login
$ firebase emulators:start
```

## Dependências

- Framework do backend: `node`, `firebase-tools`, `firebase-functions`, `express`, `body-parser`, `babel`.
- Validação das requisições: `express-validator`.
- Autenticação: `firebase-admin`.
- Banco de Dados: `firestore`, `postgresql`.
- Padronização código: `eslint`.

## Funcionalidades/Rotas 🏗

- PUT /api/v1/auth/signup
  - recebe parametros email e password e retorna objeto usuário.
- POST /api/v1/auth/login
  - recebe parametros email e password e retorna token de autenticação.
- GET /api/v1/auth/logout
  - desloga usuário.
- GET /api/v1/users/current
  - usuario corrente
- GET /api/v1/users/all
  - lista usuarios
- POST /api/v1/transactions/send-database
  - transfere creditos tipo database do usuario corrente para o usuario informado
- POST /api/v1/transactions/send-firestore
  - transfere creditos tipo firestore do usuario corrente para o usuario informado

## Características

## Estrutura

```
+-- functions/
|	+-- src/
|	|	+-- routes/
|	|	|	+-- authentication.js
|	|	|	+-- index.js
|	|	|	+-- transactions.js
|	|	|	+-- users.js
|	|	+-- services/
|	|	|   +-- authentication/
|	|	|	|   +-- ensureAuthenticated.js
|	|	|	|   +-- index.js
|	|	|	|   +-- login.js
|	|	|	|   +-- logout.js
|	|	|	|   +-- signup.js
|	|	|   +-- transactions/
|	|	|	|   +-- index.js
|	|	|	|   +-- sendCreditsDatabase.js
|	|	|	|   +-- sendCreditsFirestore.js
|	|	|   +-- users/
|	|	|	|   +-- current.js
|	|	|	|   +-- index.js
|	|	|	|   +-- list.js
|	|	+-- server.js
|	+-- .babelrc
|	+-- .eslintrc.json
|	+-- .gitignore
|	+-- cors.json
|	+-- index.js
|	+-- package-lock.json
|	+-- package.json
+-- .firebaserc
+-- .gitignore
+-- firebase.json
+-- README.md
```
