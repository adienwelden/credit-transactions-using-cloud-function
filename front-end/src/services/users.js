import { auth } from "../firebase";
import api from "../api";

export const fetchCurrent = () => {
  return auth.currentUser
    .getIdToken(true)
    .then(token => {
      const request = {
        token,
        method: "GET",
        endpoint: "users/current"
      };
      return api(request);
    })
    .catch(err => err);
};

export const fetchAll = () => {
  return auth.currentUser
    .getIdToken(true)
    .then(token => {
      const request = {
        token,
        method: "GET",
        endpoint: "users/all"
      };
      return api(request);
    })
    .catch(err => err);
};
