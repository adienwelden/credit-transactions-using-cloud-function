import { auth } from "../firebase";
import api from "../api";

export const transferRequest = (receiver, type, value) => {
  const data = { receiver, value, type };
  return auth.currentUser
    .getIdToken(true)
    .then(token => {
      const request = {
        token,
        method: "POST",
        endpoint: `transactions/send-${type}`,
        data
      };
      return api(request);
    })
    .catch(err => err);
};

export const transferHistory = type => {
  return auth.currentUser
    .getIdToken(true)
    .then(token => {
      const request = {
        token,
        method: "GET",
        endpoint: `transactions/history/${type}`
      };
      return api(request);
    })
    .catch(err => err);
};
