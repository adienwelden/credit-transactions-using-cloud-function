import axios from "axios";
import queryString from "query-string";

import { serverConfig } from "../constants";

const api = ({ token, method, endpoint, data }) => {
  let response = { ok: false, payload: null };

  const config = {
    method,
    timeout: 5000,
    responseType: "json",
    url: `${serverConfig.apiHost}/${endpoint}`
  };

  if (method === "POST" || method === "PUT") {
    config.data = data;
  } else {
    const qs = queryString.stringify(data);
    config.url = `${config.url}/?${qs}`;
  }
  if (token) {
    config.headers = { Authorization: `Bearer ${token}` };
  }
  // console.log(config.url);

  return axios(config)
    .then(res => {
      // console.log(res.data);
      return res.data;
    })
    .catch(err => {
      if (err.response) {
        response = err.response.data;
      } else {
        response.message = err.message;
      }
      return response;
    });
};

export default api;
