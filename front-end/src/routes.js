import Layout from "./containers/Layout";
import Main from "./containers/Main";

import LayoutDash from "./containers/dashboard/LayoutDash";
import MainDash from "./containers/dashboard/MainDash";
import History from "./containers/dashboard/History";

import Signup from "./containers/auth/Signup";
import Login from "./containers/auth/Login";

const Routes = [
  {
    component: LayoutDash,
    path: "/dashboard",
    routes: [
      {
        path: "/dashboard",
        component: MainDash,
        exact: true
      },
      {
        path: "/dashboard/history/database",
        component: History,
        type: "database"
      },
      {
        path: "/dashboard/history/firestore",
        component: History,
        type: "firestore"
      }
    ]
  },
  {
    component: Layout,
    path: "/",
    routes: [
      {
        path: "/",
        component: Main,
        exact: true
      },
      {
        path: "/login",
        component: Login
      },
      {
        path: "/signup",
        component: Signup
      }
    ]
  }
];

export default Routes;
