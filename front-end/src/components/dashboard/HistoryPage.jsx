import React from "react";
import PropTypes from "prop-types";
import cuid from "cuid";

const Item = ({ item, index }) => (
  <div className="columns is-centered">
    <div className="column">{index + 1}</div>
    <div className="column">{item.email}</div>
    <div className="column has-text-centered">
      {item.type.charAt(0).toUpperCase() + item.type.slice(1)}
    </div>
    <div className="column has-text-centered">
      {Number.parseFloat(item.value).toFixed(3)}
    </div>
    <div className="column" />
  </div>
);

const HistoryPage = ({ list }) => (
  <div>
    {list.map((item, index) => {
      const key = cuid();
      return <Item key={key} item={item} index={index} />;
    })}
  </div>
);

HistoryPage.propTypes = {
  list: PropTypes.array.isRequired
};

export default HistoryPage;
