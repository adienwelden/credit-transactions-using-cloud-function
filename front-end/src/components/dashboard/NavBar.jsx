import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const NavBar = ({ user, loading, database, firestore, handleLogout }) => (
  <nav
    className="navbar is-dark"
    role="navigation"
    aria-label="main navigation"
  >
    <div className="navbar-end has-text-white">
      <div className="navbar-item has-text-white">
        <div className="buttons has-text-white">
          <div className="navbar-item has-text-white">Welcome {user.email}</div>
          <div className="navbar-item has-text-white">
            Firstore {loading ? "..." : `$${firestore}`}
          </div>
          <div className="navbar-item has-text-white">
            Database {loading ? "..." : `$${database}`}
          </div>
          <Link className="has-text-white" to="/dashboard">
            Users
          </Link>
          &nbsp;|&nbsp;
          <Link className="has-text-white" to="/dashboard/history/database">
            History DB
          </Link>
          &nbsp;|&nbsp;
          <Link className="has-text-white" to="/dashboard/history/firestore">
            History FS
          </Link>
          &nbsp;|&nbsp;
          <Link className="has-text-white" to="#" onClick={handleLogout}>
            Logout
          </Link>
        </div>
      </div>
    </div>
  </nav>
);

NavBar.propTypes = {
  database: PropTypes.string.isRequired,
  firestore: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  handleLogout: PropTypes.func.isRequired
};

export default NavBar;
