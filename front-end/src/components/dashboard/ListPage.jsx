import React from "react";
import PropTypes from "prop-types";
import cuid from "cuid";

const Button = ({ id, target, name, label, handleClick }) => {
  return (
    <button
      name={name}
      type="button"
      className="button is-small"
      onClick={e => handleClick(e, id, target, name)}
    >
      {label}
    </button>
  );
};

const Input = ({ id }) => {
  return (
    <input
      type="text"
      size="10"
      id={id}
      className="is-small item-margin-bottom"
      defaultValue=""
    />
  );
};

const Item = ({ id, item, handleClick }) => (
  <div className="columns is-centered">
    <div className="">{item.email}</div>
    <Input id={`${id}-firestore`} />
    <Button
      id={id}
      target={item}
      name="firestore"
      label="Send Firestore"
      handleClick={handleClick}
    />
    <Input id={`${id}-database`} />
    <Button
      id={id}
      target={item}
      name="database"
      label="Send Database"
      handleClick={handleClick}
    />
  </div>
);

const ListPage = ({ message, list, handleClick }) => (
  <div>
    {list.map(item => {
      const key = cuid();
      return <Item key={key} id={key} handleClick={handleClick} item={item} />;
    })}
  </div>
);

ListPage.propTypes = {
  message: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  handleClick: PropTypes.func.isRequired
};

export default ListPage;
