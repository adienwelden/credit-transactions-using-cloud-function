import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Signup = ({ email, password, message, handleRun, handleChange }) => (
  <form onSubmit={handleRun}>
    <div className="columns is-centered">
      <div className="column is-half">
        <p className="title">Signup</p>
        <div className="field">
          {message && <div className="tag">{message}</div>}
          <label className="label">Email</label>
          <div className="control">
            <input
              className="input"
              value={email}
              onChange={handleChange}
              type="email"
              name="email"
              id="email"
              placeholder="Email"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Password</label>
          <div className="control">
            <input
              className="input"
              value={password}
              onChange={handleChange}
              type="password"
              name="password"
              id="password"
              placeholder="Password"
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <button className="button is-link" id="run" type="submit">
              Signup
            </button>
          </div>
        </div>
        <Link to="/login">Login</Link>
      </div>
    </div>
  </form>
);

Signup.propTypes = {
  handleRun: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired
};

Signup.displayName = "SignupView";

export default Signup;
