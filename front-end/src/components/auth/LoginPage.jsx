import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Login = ({ email, password, message, handleRun, handleChange }) => (
  <form onSubmit={handleRun}>
    <div className="columns is-centered">
      <div className="column is-half">
        <p className="title">Login</p>
        <div className="field">
          {message && <div className="tag">{message}</div>}
          <label className="label">Email</label>
          <div className="control">
            <input
              className="input"
              value={email}
              onChange={handleChange}
              type="email"
              name="email"
              id="email"
              placeholder="Email"
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Password</label>
          <div className="control">
            <input
              className="input"
              value={password}
              onChange={handleChange}
              type="password"
              name="password"
              id="password"
              placeholder="Password"
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <button className="button is-link" id="run" type="submit">
              Login
            </button>
          </div>
        </div>
        <Link to="/signup">Signup</Link>
      </div>
    </div>
  </form>
);

Login.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  handleRun: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired
};

Login.displayName = "LoginView";

export default Login;
