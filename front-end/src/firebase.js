import * as firebase from "firebase/app";
import "firebase/auth";

import { firebaseConfig } from "./constants";

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
