import React from "react";
import { renderRoutes } from "react-router-config";

import routes from "./routes";

import "bulma/css/bulma.css";
import "./App.css";

const App = () => <div>{renderRoutes(routes)}</div>;

export default App;
