import React, { Component } from "react";
import { renderRoutes } from "react-router-config";
import { Link } from "react-router-dom";

class Layout extends Component {
  render() {
    return (
      <div>
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <Link to="/login" className="button is-primary">
                  Login
                </Link>
                <Link to="/signup" className="button is-primary">
                  Signup
                </Link>
              </div>
            </div>
          </div>
        </nav>
        <section className="section">
          {renderRoutes(this.props.route.routes)}
        </section>
      </div>
    );
  }
}

export default Layout;
