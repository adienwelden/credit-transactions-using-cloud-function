import React, { Component } from "react";

import api from "../../api";
import { auth } from "../../firebase";
import SignupPage from "../../components/auth/SignupPage";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "", message: "" };
  }

  createAccount = credential =>
    auth
      .createUserWithEmailAndPassword(credential.email, credential.password)
      .then(userAuth => {
        return auth.currentUser.getIdToken(true);
      })
      .then(token => {
        const user = auth.currentUser;
        const request = {
          token,
          method: "PUT",
          endpoint: "auth/signup",
          data: { email: user.email, uid: user.uid }
        };
        return api(request);
      })
      .then(response => {
        console.log("response crateAccount:", response);
        const { history } = this.props;
        history.push("/login");
      })
      .catch(err => {
        this.setState({ message: err.message });
      });

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleRun = event => {
    event.preventDefault();
    const credential = { ...this.state };
    credential.password = credential.password.trim();
    credential.email = credential.email.trim();
    credential.email = credential.email.toLowerCase();
    this.createAccount(credential);
  };

  render() {
    const { email, password, message } = this.state;
    return (
      <SignupPage
        email={email}
        password={password}
        message={message}
        handleRun={this.handleRun}
        handleChange={this.handleChange}
      />
    );
  }
}

Signup.displayName = "Signup";

export default Signup;
