import React, { Component } from "react";

import { auth } from "../../firebase";
import LoginPage from "../../components/auth/LoginPage";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "", message: "" };
  }

  authenticate = credential => {
    const { history } = this.props;
    auth
      .signInWithEmailAndPassword(credential.email, credential.password)
      .then(userAuth => auth.currentUser.getIdToken(true))
      .then(token => {
        history.push("/dashboard");
      })
      .catch(err => {
        this.setState({ message: err.message });
      });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleRun = event => {
    event.preventDefault();
    const credential = { ...this.state };
    credential.password = credential.password.trim();
    credential.email = credential.email.trim();
    credential.email = credential.email.toLowerCase();
    this.authenticate(credential);
  };

  render() {
    const { email, password, message } = this.state;
    return (
      <LoginPage
        email={email}
        password={password}
        message={message}
        handleRun={this.handleRun}
        handleChange={this.handleChange}
      />
    );
  }
}

Login.displayName = "Login";

export default Login;
