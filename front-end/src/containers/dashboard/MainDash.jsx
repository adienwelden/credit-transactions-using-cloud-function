import React, { Component } from "react";

import ListPage from "../../components/dashboard/ListPage";

import { transferRequest } from "../../services/transfer";
import { fetchAll as fetchAllUsers } from "../../services/users";

class MainDash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      list: [],
      message: ""
    };
  }

  componentDidMount() {
    fetchAllUsers()
      .then(res => {
        if (res.ok) {
          this.setState({ list: res.payload });
        }
        this.setState({ loading: false });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  }

  handleClick = (event, id, receiver, type) => {
    event.preventDefault();
    const input = document.getElementById(`${id}-${type}`);
    let value = Number.parseFloat(input.value);
    if (Number.isNaN(value) || !value) {
      alert("Please type a valid value.");
      return;
    }
    value = value.toFixed(3);
    const msg = `Transfering ${value} to ${receiver.email} by ${type}`;
    alert(msg);
    transferRequest(receiver, type, value).then(response => {
      alert(response.message);
      const { updateNav } = this.props;
      updateNav();
    });
  };

  render() {
    const { loading, message, list } = this.state;
    if (loading) return <div>Loading...</div>;
    if (!list.length) return <div>list is empty.</div>;
    return (
      <ListPage message={message} list={list} handleClick={this.handleClick} />
    );
  }
}

export default MainDash;
