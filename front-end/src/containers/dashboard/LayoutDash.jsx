import React, { Component } from "react";
import { renderRoutes } from "react-router-config";

import { auth } from "../../firebase";
import { fetchCurrent as fetchCurrentUser } from "../../services/users";

import NavBar from "../../components/dashboard/NavBar";

class LayoutDash extends Component {
  constructor(props) {
    super(props);
    this.state = { user: null, loading: true };
  }

  componentDidMount() {
    const { history } = this.props;
    auth.onAuthStateChanged(user => {
      if (!user || !auth.currentUser) {
        history.push("/login");
        return;
      }
      this.updateNav();
    });
  }

  handleLogout = () => {
    const { history } = this.props;
    auth.signOut().then(() => {
      history.push("/login");
    });
  };

  updateNav = () => {
    this.setState({ loading: true });
    fetchCurrentUser().then(res => {
      if (res.ok) {
        this.setState({ user: res.payload });
      }
      this.setState({ loading: false });
    });
  };

  render() {
    const { user, loading } = this.state;
    if (!user) return <div>Loading...</div>;
    const firestore = Number.parseFloat(user.firestoreCredits).toFixed(3);
    const database = Number.parseFloat(user.databaseCredits).toFixed(3);
    return (
      <div>
        <NavBar
          user={user}
          loading={loading}
          firestore={firestore}
          database={database}
          handleLogout={this.handleLogout}
        />
        <section className="section">
          {renderRoutes(this.props.route.routes, { updateNav: this.updateNav })}
        </section>
      </div>
    );
  }
}

export default LayoutDash;
