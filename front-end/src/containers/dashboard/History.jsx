import React, { Component } from "react";

import HistoryPage from "../../components/dashboard/HistoryPage";

import { transferHistory } from "../../services/transfer";

class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      list: [],
      message: ""
    };
  }

  componentDidMount() {
    const { type } = this.props.route;
    transferHistory(type)
      .then(res => {
        if (res.ok) {
          this.setState({ list: res.payload });
        }
        this.setState({ loading: false });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  }

  render() {
    const { loading, list } = this.state;
    if (loading) return <div>Loading...</div>;
    if (!list.length) return <div>list is empty.</div>;
    return <HistoryPage list={list} />;
  }
}

export default History;
