require("babel-register")({
  presets: ["env"]
});

const server = require("./src/server");

exports.api = server.api;
