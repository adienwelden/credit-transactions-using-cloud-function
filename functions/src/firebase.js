import * as admin from "firebase-admin";

import serviceAccount from "./constants/serviceAccountKey.json";

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://proj-d6730.firebaseio.com"
});

admin.firestore().settings({
  timestampsInSnapshots: true
});

export const usersAuth = admin.auth();

export const firestore = admin.firestore();

export const usersDoc = firestore.collection("users");

export const historyDoc = firestore.collection("history");
