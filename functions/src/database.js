import { Pool } from "pg";

const database = new Pool({
  host: "localhost",
  user: "user",
  database: "projdb",
  password: "pass",
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000
});

export { database };
