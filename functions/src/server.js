import * as functions from "firebase-functions";
import Express from "express";
import bodyParser from "body-parser";
// import expressValidator from "express-validator";

import cors from "../cors.json";
import * as routes from "./routes";

const app = new Express();

app.use(bodyParser.json({ limit: "20mb" }));
app.use(bodyParser.urlencoded({ limit: "20mb", extended: false }));
// app.use(expressValidator());

app.use((req, res, next) => {
  res.header(cors);
  next();
});

app.use("/v1/auth", routes.authentication);
app.use("/v1/users", routes.users);
app.use("/v1/transactions", routes.transactions);

export const api = functions.https.onRequest(app);
