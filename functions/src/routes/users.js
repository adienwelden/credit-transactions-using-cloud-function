import Express from "express";

import ensureAuthenticated from "../services/authentication/ensureAuthenticated";
import * as users from "../services/users";

const router = Express.Router();

router.route("/current").get(ensureAuthenticated, users.current);

router.route("/all").get(ensureAuthenticated, users.list);

export { router as users };
