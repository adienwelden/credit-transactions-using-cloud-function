import Express from "express";

import * as services from "../services/authentication";

const router = Express.Router();

router.route("/login").post(services.login);

router.route("/logout").get(services.ensureAuthenticated, services.logout);

router.route("/signup").put(services.signup);

export { router as authentication };
