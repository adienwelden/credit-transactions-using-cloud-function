import Express from "express";

import ensureAuthenticated from "../services/authentication/ensureAuthenticated";
import * as firestoreTs from "../services/transactions/firestore";
import * as databaseTs from "../services/transactions/database";

const router = Express.Router();

router
  .route("/history/firestore")
  .get(ensureAuthenticated, firestoreTs.history);

router.route("/history/database").get(ensureAuthenticated, databaseTs.history);

router.route("/send-database").post(ensureAuthenticated, databaseTs.transfer);

router.route("/send-firestore").post(ensureAuthenticated, firestoreTs.transfer);

export { router as transactions };
