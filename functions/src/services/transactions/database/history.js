import { database } from "../../../database";

export const createHistory = (user, data) => {
  const debit = {
    text:
      "INSERT INTO history (useremail, email, type, value) VALUES ($1, $2, $3, $4)",
    values: [user.email, data.email, data.type, data.value]
  };
  const credit = {
    text:
      "INSERT INTO history (useremail, email, type, value) VALUES ($1, $2, $3, $4)",
    values: [data.email, user.email, "credit", data.value]
  };
  return database
    .query(debit)
    .then(() => database.query(credit))
    .catch(err => err.message);
};

const history = (req, res) => {
  const response = { ok: false, payload: null };

  const { user } = req;
  const query = {
    text: `SELECT * FROM history WHERE useremail=$1 ORDER BY id DESC`,
    values: [user.email]
  };

  return database
    .query(query)
    .then(results => {
      response.ok = true;
      response.message = "database list service.";
      response.payload = results.rows;
      res.status(200).json(response);
      return;
    })
    .catch(err => {
      return err;
    });
};

export default history;
