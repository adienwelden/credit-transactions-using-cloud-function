import { database } from "../../../database";

const debit = (email, value) => {
  let clientconn;
  return database
    .connect()
    .then(client => {
      clientconn = client;
      const query = {
        text: "SELECT * FROM users WHERE email=$1",
        values: [email]
      };
      return clientconn.query(query);
    })
    .then(result => {
      const credits = result.rows[0].credits;
      if (parseFloat(credits) < parseFloat(value)) {
        return false;
      }
      const newValue = parseFloat(credits) - parseFloat(value);
      const query = {
        text: "UPDATE users SET credits=$1 WHERE email=$2",
        values: [newValue, email]
      };
      return clientconn.query(query);
    })
    .then(result => {
      if (result && result.rowCount) {
        return true;
      }
      return null;
    })
    .catch(() => {
      return "server error.";
    });
};

export default debit;
