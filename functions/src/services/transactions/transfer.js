import credit from "./credit";
import debit from "./debit";
import { createHistory } from "./history";

const transfer = (req, res) => {
  const response = { ok: false, payload: null };

  const data = req.body;

  return debit(req.user.email, data.value)
    .then(result => {
      if (result) {
        return credit(data.receiver.email, data.value);
      }
      response.message = "insuficient credits.";
      return res.status(200).json(response);
    })
    .then(result => {
      const history = {
        email: data.receiver.email,
        type: "debit",
        value: data.value
      };
      if (result) {
        return createHistory(req.user, history);
      }
      response.message = `Transfer failure on ${data.type} credits.`;
      return res.status(200).json(response);
    })
    .then(result => {
      response.ok = true;
      response.message = `Transfer complete on ${data.type} credits.`;
      response.payload = data;
      return res.status(200).json(response);
    })
    .catch(err => {
      response.message = err.message;
      return res.status(500).json(response);
    });
};

export default transfer;
