import { usersDoc } from "../../../firebase";
import { idFromEmail } from "../../users/firestore/utils";

const credit = (email, value) => {
  return idFromEmail(email)
    .then(id => {
      return usersDoc.doc(id).get();
    })
    .then(doc => {
      if (doc.exists) {
        const data = doc.data();
        const credits = data.credits;
        const newValue = parseFloat(credits) + parseFloat(value);
        return usersDoc.doc(doc.id).update({ credits: newValue });
      }
      throw new Error(null);
    })
    .then(result => {
      if (result) {
        return true;
      }
      return false;
    })
    .catch(err => err);
};

export default credit;
