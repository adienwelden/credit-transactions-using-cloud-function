import { firestore, historyDoc } from "../../../firebase";
import { idFromEmail } from "../../users/firestore/utils";

export const createHistory = (user, data) => {
  const userHistory = historyDoc.doc(user.uid).collection("transations");
  return userHistory
    .add(data)
    .then(() => {
      return idFromEmail(data.email);
    })
    .then(id => {
      const userHistory = historyDoc.doc(id).collection("transations");
      data.type = "credit";
      return userHistory.add(data);
    })
    .catch(err => err.message);
};

const history = (req, res) => {
  const response = { ok: false, payload: null };
  const { user } = req;

  const userHistory = historyDoc.doc(user.uid).collection("transations");

  return userHistory
    .listDocuments()
    .then(documentRefs => firestore.getAll(...documentRefs))
    .then(documentSnapshots => {
      const history = [];
      for (let documentSnapshot of documentSnapshots) {
        if (documentSnapshot.exists) {
          history.push(documentSnapshot.data());
        }
      }
      response.ok = true;
      response.message = "list service.";
      response.payload = history;
      res.status(200).json(response);
      return;
    });
};

export default history;
