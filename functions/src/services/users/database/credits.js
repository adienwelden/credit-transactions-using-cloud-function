import { database } from "../../../database";

export const credits = uid => {
  const query = {
    text: "SELECT * FROM users WHERE uid=$1",
    values: [uid]
  };
  return database
    .query(query)
    .then(result => {
      return result.rows[0].credits;
    })
    .catch(err => {
      return err;
    });
};

export default credits;
