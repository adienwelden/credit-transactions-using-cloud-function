import { database } from "../../../database";

export const create = (uid, data) => {
  const query = {
    text: "INSERT INTO users (uid, email, credits) VALUES ($1, $2, $3)",
    values: [uid, data.email, data.credits]
  };
  return database
    .query(query)
    .then(result => {
      return result.insertId;
    })
    .catch(err => {
      return err;
    });
};

export default create;
