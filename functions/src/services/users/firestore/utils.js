import { usersDoc } from "../../../firebase";

const idFromEmail = email => {
  return usersDoc
    .where("email", "==", email)
    .get()
    .then(docs => {
      const res = [];
      docs.forEach(doc => {
        res.push(doc.id);
      });
      if (res.length === 1) {
        return res[0];
      }
      throw new Error("document not found or duplicated.");
    })
    .catch(err => err);
};

export { idFromEmail };
