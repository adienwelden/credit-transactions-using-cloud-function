import { usersDoc } from "../../../firebase";

export const create = (id, data) => {
  const docRef = usersDoc.doc(id);
  return docRef.set(data);
};

export default create;
