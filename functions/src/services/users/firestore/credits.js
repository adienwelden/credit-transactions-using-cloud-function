import { usersDoc } from "../../../firebase";

const credits = id => {
  const ref = usersDoc.doc(id);
  return ref
    .get()
    .then(doc => {
      if (doc.exists) {
        return doc.get("credits");
      }
      throw new Error("document not found.");
    })
    .catch(err => err);
};

export default credits;
