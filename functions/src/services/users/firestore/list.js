import { firestore, usersDoc } from "../../../firebase";

const list = (req, res) => {
  const response = { ok: false, payload: null };

  return usersDoc
    .listDocuments()
    .then(documentRefs => firestore.getAll(...documentRefs))
    .then(documentSnapshots => {
      const users = [];
      let data;
      for (let documentSnapshot of documentSnapshots) {
        if (documentSnapshot.exists) {
          data = documentSnapshot.data();
          users.push({ email: data.email });
        }
      }
      const payload = users.filter(item => item.email !== req.user.email);
      response.ok = true;
      response.message = "list service.";
      response.payload = payload;
      res.status(200).json(response);
      return;
    });
};

export default list;
