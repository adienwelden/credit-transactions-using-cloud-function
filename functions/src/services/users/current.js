import * as firestoreInfo from "./firestore";
import * as databaseInfo from "./database";

const current = (req, res) => {
  const response = { ok: false, payload: null };

  const { user } = req;
  let firestoreCredits = 0;

  return firestoreInfo
    .credits(user.uid)
    .then(credits => {
      firestoreCredits = credits;
      return databaseInfo.credits(user.uid);
    })
    .then(credits => {
      const userData = {
        email: user.email,
        databaseCredits: credits,
        firestoreCredits
      };
      response.ok = true;
      response.message = "current service.";
      response.payload = userData;
      res.status(200).json(response);
      return;
    })
    .catch(() => {
      response.message = "Forbidden.";
      res.status(500).json(response);
    });
};

export default current;
