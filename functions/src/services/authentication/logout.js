const logout = (req, res) => {
  const response = { ok: false, payload: null };

  response.ok = true;
  response.message = "logout service.";

  return res.status(200).json(response);
};

export default logout;
