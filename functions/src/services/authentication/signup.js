import * as users from "../users/firestore";
import * as database from "../users/database";

const signup = (req, res) => {
  const response = { ok: false, payload: null };

  const data = req.body;
  const user = {
    email: data.email,
    credits: 100.0
  };

  return users
    .create(data.uid, user)
    .then(result => {
      return database.create(data.uid, user);
    })
    .then(result => {
      response.ok = true;
      response.message = "signup service.";
      response.payload = user;
      res.status(200).json(response);
      return;
    })
    .catch(() => {
      response.message = "signup service error.";
      res.status(200).json(response);
      return;
    });
};

export default signup;
