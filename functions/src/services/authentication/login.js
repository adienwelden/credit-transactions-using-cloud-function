const login = (req, res) => {
  const response = { ok: false, payload: null };

  const credentials = req.body;

  response.ok = true;
  response.message = "login service.";
  response.payload = credentials;

  return res.status(200).json(response);
};

export default login;
