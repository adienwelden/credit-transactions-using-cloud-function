import { usersAuth } from "../../firebase";

const ensureAuthenticated = (req, res, next) => {
  const token =
    (req.headers.authorization && req.headers.authorization.split(" ")[1]) ||
    null;
  if (token) {
    usersAuth
      .verifyIdToken(token)
      .then(decoded => {
        req.user = {
          uid: decoded.uid,
          email: decoded.email
        };
        next();
        return;
      })
      .catch(() => {
        res.status(500).send({ ok: false, message: "Forbidden." });
      });
  } else {
    res.status(500).send({ ok: false, message: "Forbidden." });
    return;
  }
};

export default ensureAuthenticated;
